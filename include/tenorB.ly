tenorB = \relative c' {
	\set Staff.instrumentName = "Tenor B"

	%% A %%
	e1*4
	%% B %%
	a1*4
}
